package com.mogan;
import java.lang.Math;
public class Vector {
    double[] vec;
    
    public Vector(double x, double y, double z){
        vec = new double[]{x, y, z};
    }

    public void print(){
        System.out.println(vec[0] + ", " + vec[1] + ", " + vec[2]);
    }

    public double mag(){
        double someValue = vec[0]*vec[0] + vec[1]*vec[1] + vec[2]*vec[2];
        double magnitude = Math.sqrt(someValue);
        return magnitude;
    }

    public Vector add(Vector other){
        double x_ans = vec[0] + other.vec[0];
        double y_ans = vec[1] + other.vec[1];
        double z_ans = vec[2] + other.vec[2];
        Vector someVector = new Vector(x_ans, y_ans, z_ans);
        return someVector;
    }
}